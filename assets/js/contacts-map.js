(function() {
  

	var myMap;
	var defaultCenter = [59.6044, 37.6365];
	var defaultZoom = 3;
	var globalJson;
	var mySlimSelect;
	
	if($('#contacts-map').length==0) { return false; }
	
	function changeCity(CITY_ID, DEF_PLACEMARK) {
		
		var zoom = 11;
		var center;
		var placemark;
		
		$('.region-info').empty();
		
		for (var prop in globalJson) {
			
			if(globalJson[prop].id==2) { continue; }
			
			if(globalJson[prop].city_id == CITY_ID) { 
				
				center = [globalJson[prop].lat,globalJson[prop].lng]; 	
				
				if(CITY_ID == '2') {
					placemark = false;
					zoom = 9;
					center = [55.751283277080105,37.62483958203125];
				} else if(CITY_ID == '15') {
					placemark = false;
					zoom = 9;
					center = [59.939924022042625,30.320182822885375];
				} else {
							
					placemark = globalJson[prop].placemark;
				}
				
				var html = '\
				<div class="col pb-5 pt-5">\
					<div class="h3 pb-3">'+globalJson[prop].name_ru+'</div>\
					<div class="ic contacts-phone-ic pb-2"><b>Телефон:</b> <a href="tel:'+globalJson[prop].m_phone+'">'+globalJson[prop].m_phone+'</a></div>\
					<div class="ic contacts-email-ic pb-2"><b>Email:</b> <a href="mailto:'+globalJson[prop].m_email+'">'+globalJson[prop].m_email+'</a></div>\
					<div class="ic contacts-map-ic pb-2"><b>Адрес:</b> '+globalJson[prop].m_address_ru+'</div>\
				</div>';
				$('.region-info').append(html);
				
			}
		}
		
		if(typeof DEF_PLACEMARK != 'undefined') { placemark = DEF_PLACEMARK; }
		
		myMap.setZoom(zoom);
		myMap.setCenter(center);
		if(placemark) { placemark.balloon.open(); }
		
	}

	$(document).on('click','.contacts-list li a',function(e){
		
		e.preventDefault();
		$('.contacts-list li a').removeClass('active');
		$(this).addClass('active');
		if(typeof myMap == 'undefined') { return true; } 
				
		changeCity($(this).data('id'));
		
	});

	

			$.getScript( "https://api-maps.yandex.ru/2.1/?lang=ru_RU", function( data, textStatus, jqxhr ) {
			  createContactsMap();
			  
			});
			

	
	
		
		function createContactsMap() {
			$.ajax({
			dataType: "json",
			url: '/assets/map.json',
			type: 'get',
			success: function(jsonData) {
				
				globalJson = jsonData;
				console.log(globalJson);
				
								
				if(typeof ymaps == 'undefined') { $('#contacts-map').remove(); return; }
						
				ymaps.ready(function(){
					
					myMap = new ymaps.Map("contacts-map", {
					center: defaultCenter,
					controls: ['zoomControl'],
					zoom: defaultZoom
					});
					
					if( window.navigator.userAgent.indexOf("MSIE ") > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {  
						var marker = { preset: "islands#orangeStretchyIcon" };
					} else {
						var marker = { iconLayout: 'default#image', iconImageHref: '/assets/img/marker.svg' };
					}
					
					
					jsonData.forEach(function(elt, index){
						
						var baloon = '';
						if(typeof elt.m_phone!='undefined') {
							baloon += "<b>Телефон</b>: <a href='tel:"+elt.m_phone.replace(/[^\w\s]/gim, '')+"'>"+elt.m_phone+"</a><br/>";
						}
						if(typeof elt.m_email!='undefined') {
							baloon += "<b>Email</b>: <a href='mailto:"+elt.m_email+"'>"+elt.m_email+"</a><br/>";
						}
						if(typeof elt.m_address_ru!='undefined') {
							baloon += "<b>Адрес</b>: "+elt.m_address_ru;
						}					
						
						
						var myPlacemark = new ymaps.Placemark([elt.lat, elt.lng], {
							balloonContentHeader: elt.name_ru,
							balloonContentBody: baloon,
							balloonContentFooter: "",
							hintContent: elt.name_ru,
							clusterCaption: elt.name_ru,
							id:	elt.id,
							city_id: elt.city_id	
						},marker);
						
						
											
						
						myMap.geoObjects.add(myPlacemark); 
						
						globalJson[index].placemark = myPlacemark;
						
						myPlacemark.events.add('click', function (e) {
							
							changeCity(myPlacemark.properties.get("city_id"), myPlacemark);
							
						});
						
					});	
											
						
					myMap.behaviors.disable('scrollZoom');
					
					
				});
				
				
			},
			error: function(data) {
				 $('#checkout-map').remove();
			}
			});
		}
	
	
})();	