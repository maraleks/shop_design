(function() {
  

	var myMap;
	var defaultCenter = [59.6044, 37.6365];
	var defaultZoom = 3;
	var globalJson;
	var mySlimSelect;
	
	if($('#checkout-map').length==0) { return false; }
	
	

	$(document).on('change','#choose-stock',function(){
		
		if(typeof myMap == 'undefined') { return true; } 
		if($(this).val()=='Не выбрано') {
			myMap.setZoom(defaultZoom);
			myMap.setCenter(defaultCenter);
			for (var prop in globalJson) {
				globalJson[prop].placemark.balloon.close();
			}
			return;
		}
		
		var zoom = 11;
		var center;
		var placemark;
		
		for (var prop in globalJson) {
			if(globalJson[prop].id == $(this).val()) { 
				center = [globalJson[prop].lat,globalJson[prop].lng]; 			
				placemark = globalJson[prop].placemark;
				break; 
			}
		}
						
		myMap.setZoom(zoom);
		myMap.setCenter(center);
		if(typeof placemark != 'undefined') { placemark.balloon.open(); }
		
	});

	
	$(document).on('click', '.checkout-pickup', function () { 
		
		
			if(typeof myMap != 'undefined') { return true; } 

			$.getScript( "https://api-maps.yandex.ru/2.1/?lang=ru_RU", function( data, textStatus, jqxhr ) {
			  createCheckoutMap();
			  
			});
			

	});
	
		
		function createCheckoutMap() {
			$.ajax({
			dataType: "json",
			url: '/assets/map.json',
			type: 'get',
			success: function(jsonData) {
				
				globalJson = jsonData;
				console.log(globalJson);
				
				if($('#choose-stock').length>0) {
					
					var selData = [];
					selData.push({text: 'Не выбрано', value: 0, selected: true});
					for (var prop in jsonData) {
						var reg = {};
						reg.text = jsonData[prop].name_ru;
						reg.value = jsonData[prop].id;
						
						selData.push(reg);
					}
					
					mySlimSelect = new SlimSelect({
						  select: '#choose-stock',
						  data: selData,
						  placeholder: 'Выберите вариант',
						  searchPlaceholder: 'Поиск',
						  allowDeselectOption: true
						});
					
					
				}
				
				if(typeof ymaps == 'undefined') { $('#checkout-map').remove(); return; }
						
				ymaps.ready(function(){
					
					myMap = new ymaps.Map("checkout-map", {
					center: defaultCenter,
					controls: ['zoomControl'],
					zoom: defaultZoom
					});
					
					if( window.navigator.userAgent.indexOf("MSIE ") > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {  
						var marker = { preset: "islands#orangeStretchyIcon" };
					} else {
						var marker = { iconLayout: 'default#image', iconImageHref: '/assets/img/marker.svg' };
					}
					
					
					jsonData.forEach(function(elt, index){
						
						var baloon = '';
						if(typeof elt.m_phone!='undefined') {
							baloon += "<b>Телефон</b>: <a href='tel:"+elt.m_phone.replace(/[^\w\s]/gim, '')+"'>"+elt.m_phone+"</a><br/>";
						}
						if(typeof elt.m_email!='undefined') {
							baloon += "<b>Email</b>: <a href='mailto:"+elt.m_email+"'>"+elt.m_email+"</a><br/>";
						}
						if(typeof elt.m_address_ru!='undefined') {
							baloon += "<b>Адрес</b>: "+elt.m_address_ru;
						}					
						
						
						var myPlacemark = new ymaps.Placemark([elt.lat, elt.lng], {
							balloonContentHeader: elt.name_ru,
							balloonContentBody: baloon,
							balloonContentFooter: "",
							hintContent: elt.name_ru,
							clusterCaption: elt.name_ru,
							id:	elt.id						
						},marker);
						
						
						
						
						
						myMap.geoObjects.add(myPlacemark); 
						
						globalJson[index].placemark = myPlacemark;
						
						myPlacemark.events.add('click', function (e) {
							console.log(myPlacemark.properties.get("id"));
							mySlimSelect.set(myPlacemark.properties.get("id"));
						});
						
					});	
											
						
					
					
					
				});
				
				
			},
			error: function(data) {
				 $('#checkout-map').remove();
			}
			});
		}
	
	
})();	