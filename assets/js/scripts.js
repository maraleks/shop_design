function setCursorPosition(pos, elem) {
        elem.focus();
        if (elem.setSelectionRange) elem.setSelectionRange(pos, pos);
        else if (elem.createTextRange) {
            var range = elem.createTextRange();
            range.collapse(true);
            range.moveEnd("character", pos);
            range.moveStart("character", pos);
            range.select()
        }
    }
	
function mask(event) {

        var allowedRange = ['800','801','802','803','804','805','806','807','808','809','811','812','813','814','815','816','817','818','820','821','831','833','834','835','836','841','842','843','844','845','846','847','848','851','855','861','862','863','865','866','867','869','871','872','873','877','878','879'];

        var matrix = "+7 (___) ___ __ __",
            i = 0,
            def = matrix.replace(/\D/g, ""),
            val = this.value.replace(/\D/g, "");



        if (def.length >= val.length) val = def;
        this.value = matrix.replace(/./g, function(a) {

            return /[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? "" : a


        });

        var code = this.value.charAt(4)+this.value.charAt(5)+this.value.charAt(6);


        if(val.length>11 && this.value.charAt(4)=='8' && allowedRange.indexOf(code) == -1) {

            this.value = this.value.replaceAt(4, val.charAt(2))
                .replaceAt(5, val.charAt(3))
                .replaceAt(6, val.charAt(4))
                .replaceAt(9, val.charAt(5))
                .replaceAt(10, val.charAt(6))
                .replaceAt(11, val.charAt(7))
                .replaceAt(13, val.charAt(8))
                .replaceAt(14, val.charAt(9))
                .replaceAt(16, val.charAt(10))
                .replaceAt(17, val.charAt(11));

        }

        if (event.type == "blur") {
            if (this.value.length == 2) this.value = ""
        } else if (event.type == "focusout") {
            if (this.value.length != 18) this.value = ""

        } else setCursorPosition(this.value.length, this)
};
	
window.addEventListener("DOMContentLoaded", function() {


    var input = document.querySelectorAll('input[type="tel"]');

	[].forEach.call(input, function(element) {	
		element.addEventListener("input", mask, false);
		element.addEventListener("focus", mask, false);
		element.addEventListener("blur", mask, false);
		element.addEventListener("focusout", mask, false);
	});
    

});


jQuery(document).ready(function( $ ) {
      
	
	
	 /* placeholder hide onclick */
	 $.Placeholder.init({ color : "#aaa" });
	 
	 /* main mobile menu */	
	 $("#mobile-menu ul").css('display','block');
	 $("#mobile-menu").mmenu();
	 

	 /* custom select */	
	 $('.custom-select').each(function(){
		var $this = $(this), numberOfOptions = $(this).children('option').length;
	  
		$this.addClass('select-hidden'); 
		$this.wrap('<div class="select"></div>');
		$this.after('<div class="select-styled"></div>');

		var $styledSelect = $this.next('div.select-styled');
		$styledSelect.text($this.children('option').eq(0).text());
	  
		var $list = $('<ul />', {
			'class': 'select-options'
		}).insertAfter($styledSelect);
	  
		for (var i = 0; i < numberOfOptions; i++) {
			$('<li />', {
				text: $this.children('option').eq(i).text(),
				rel: $this.children('option').eq(i).val()
			}).appendTo($list);
		}
	  
		var $listItems = $list.children('li');
	  
		$styledSelect.click(function(e) {
			e.stopPropagation();
			$('div.select-styled.active').not(this).each(function(){
				$(this).removeClass('active').next('ul.select-options').hide();
			});
			$(this).toggleClass('active').next('ul.select-options').toggle();
		});
	  
		$listItems.click(function(e) {
			e.stopPropagation();
			$styledSelect.text($(this).text()).removeClass('active');
			$this.val($(this).attr('rel'));
			$($this.attr('data-target')).attr('placeholder', $($this.attr('data-target')).attr('data-'+$(this).attr('rel')+'-title'));
			
			$list.hide();
		});
	  
		$(document).click(function() {
			$styledSelect.removeClass('active');
			$list.hide();
		});

	});

	
	/* main desctop menu */
	$(document).on('click', '.catalog-btn', function(){
    	
		if($(this).hasClass('mob') && $(window).height()<750) {
			return false;
		}
		
		if ( !$(this).hasClass('opened') ) {
    		$(this).addClass('opened');
    		$('.top-menu').addClass('opened');
    	} else {
    		$(this).removeClass('opened');
    		$('.top-menu').removeClass('opened');
			$('.top-menu li').removeClass('is-visible');
    	}
    	return false;
    });
	$(document).mouseup(function (e){
        var div = $('.top-menu, .catalog-btn');
        if (!div.is(e.target) && div.has(e.target).length === 0) {
            $('.top-menu, .catalog-btn').removeClass('opened');
			$('.top-menu li').removeClass('is-visible');
        }
    });
	$('.top-menu .submenu_arrow>a').mouseover(function(){
    	$(this).parent().addClass('is-visible');
        $(this).parent().siblings().removeClass('is-visible');
    });

	
	/* back-to-top */
	if ($('#back-to-top').length) {
		var scrollTrigger = 100, // px
			backToTop = function () {
				var scrollTop = $(window).scrollTop();
				if (scrollTop > scrollTrigger) {
					$('#back-to-top').addClass('show');
				} else {
					$('#back-to-top').removeClass('show');
				}
			};
		backToTop();
		$(window).on('scroll', function () {
			backToTop();
		});
		$('#back-to-top').on('click', function (e) {
			e.preventDefault();
			$('html,body').animate({
				scrollTop: 0
			}, 700);
		});
	}
	
	/* home slider */
	if($('.home-slider').length) {
		$('.home-slider').slick({
			infinite: true,
			dots: true,
			autoplay: true,
			autoplaySpeed: 5000,
			arrows: true,
			adaptiveHeight: true,
			responsive: [
				{
				  breakpoint: 991,
				  settings: {
					arrows: false,
					autoplay: false,
					fade: true,
				  }
				}
			]
		});
		$('.home-slider').show();
	}
	
	/* fix main menu on scroll */
	var fixMenu = function () { 
		
		var that = this;
		this.active = false;
		
		if($(window).outerWidth()<=991) { return true; } 
		
		if($(window).height()>750) { $('.catalog-btn.mob').removeAttr('href'); }
		
		var scrollTop = $(window).scrollTop(); 
		if (scrollTop > 125) { 
			
			$('.fixed-menu').addClass('fixed');
			$('.top-menu, .catalog-btn').removeClass('opened');
			$('.top-menu li').removeClass('is-visible');

			$('.fixed-catalog').removeClass('col-md-3').addClass('col-md-1');
			//$('.fixed-search').removeClass('col-md-9').addClass('col-md-7');
			$('.fixed-cart').removeClass('d-none');	

			that.active = true;	
			
		} else { 
			
			$('.fixed-menu').removeClass('fixed');
			
			$('.fixed-catalog').removeClass('col-md-1').addClass('col-md-3');
			//$('.fixed-search').removeClass('col-md-7').addClass('col-md-9');
			$('.fixed-cart').addClass('d-none');

			that.active = false;	
		} 
	
	}; 
	$(window).on('scroll', function () { fixMenu(); }); 
	
	
	/* show mini-cart */
	if($(window).outerWidth()>991) {
		var timeoutHandle;
		$('.cart-hover, .mini-cart').mouseover(function(){
			window.clearTimeout(timeoutHandle);
			$('.mini-cart').stop().fadeIn();
		});
		$('.cart-hover, .mini-cart').mouseout(function(e){
			timeoutHandle = window.setTimeout(function(){
				$('.mini-cart').stop().fadeOut();
			},300);
		});
		var mini_content = OverlayScrollbars(document.getElementById('mini-cart-content'), { 
			autoUpdate:true,
			overflowBehavior : {
				x: 'hidden'
			}	
		});
	}
	
	
	/* show search helper */
	$('#main-search').on('focus',function(){ $('.header-search-helper').fadeIn(); }); 
	$('#main-search').on('focusout',function(){ $('.header-search-helper').fadeOut(); });
	$(document).on('click','#search-mobile',function(){
		$('.fixed-search').removeClass('d-none');
		$('body').css('overflow','hidden');
	});
	$(document).on('click','.search-mobile-close',function(){
		$('.fixed-search').addClass('d-none');
		$('body').css('overflow','auto');
	});
	var srch_wrapper = OverlayScrollbars(document.getElementById('srch-wrapper'), { autoUpdate:true, overflowBehavior : {
				x: 'hidden'
			} });
	
	
	$('.jivo').on('click',function(){
		$(this).addClass('d-none');
		$(this).next().removeClass('d-none');
	});
	$('.jivo-open').on('click',function(){
		$(this).addClass('d-none');
		$(this).prev().removeClass('d-none');
	});
});




jQuery(document).ready(function( $ ) {
	
	/* pushbar */
	const pushbar = new Pushbar({
		blur: true,
		overlay: true
	});
	
	 /* tips */
	$(function(){
	  $('[data-toggle="tooltip"]').jTippy({size: 'tiny'});
	});
	
	/* to wish list */
	$(document).on('click', '.to-wish-list', function(){
		
		var its = $(this);
		if(its.hasClass('product-wish')) { 
			if(its.hasClass('in')) { 
				its.removeClass('in');
				its.find('span').text(its.data('in'));	
			} else { 
				its.addClass('in');
				its.find('span').text(its.data('out'));		
			}
			its = its.find('.fa-heart');  
		}
		
		if(its.hasClass('far')) {
			its.removeClass('far').addClass('fas');
			its.attr('title', its.data('out'));
		} else {
			its.removeClass('fas').addClass('far');
			its.attr('title', its.data('in'));
		}
		$('[data-toggle="tooltip"]').jTippy({size: 'tiny'});
	});
	
	/* add to cart */
	$(document).on('click', '.product-item-buy', function(){
		if($(this).hasClass('in-cart')) { return; }
		$(this).hide();
		$(this).next().removeClass('d-none');
	});
	
	
	/* add to cart counter */
	$(document).on('click', '.minus', function () {
        let $input = $(this).parent().find('.counter_input');
		let attr = $input.attr('disabled');
		if(typeof attr !== typeof undefined && attr !== false) { return; }
        let min = $input.data('min');
        let count = parseInt($input.val()) - 1;
        count = count < min ? min : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $(document).on('click', '.plus', function () {
        let $input = $(this).parent().find('.counter_input');
		let attr = $input.attr('disabled');
		if(typeof attr !== typeof undefined && attr !== false) { return; }
        let max = $input.data('max');
        let count = parseInt($input.val()) + 1;
        count = count > max ? max : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.counter_input').on('change', function(){
    	if ( $(this).val() < $(this).data('min') ) {
    		$(this).val( $(this).data('min') );
    	}
    	if ( $(this).val() > $(this).data('max') ) {
    		$(this).val( $(this).data('max') );
    	}
    });
	$('.counter_input, .filter-price-input').bind("change keyup input click", function() {
      if (this.value.match(/[^0-9]/g)) {
          this.value = this.value.replace(/[^0-9]/g, '');
      }
	});
	
	/* Change grid view */
	$(document).on('click','.change-view',function(){
		let type = $(this).data('type');
		let change = $(this).data('change');
		$('.change-view').removeClass('active');
		$(this).addClass('active');
		$('.product-items .'+type).each(function(){
			$(this).removeClass(type).addClass(change);
			Cookies.set('gridView', change, { expires: 1 });
		});
	});
	
	/* Load more */
	$(document).on('click','.btn-load-more',function(){
		
		var btn = $(this);
		var position = btn.offset().top - 83;
		
		$(this).attr('disabled',true);
		$(this).addClass('loading');
		
		setTimeout(function(){
			$('.product-items').append($('.product-items').html());
			$('html, body').animate({scrollTop: position}, 'slow');
			
			btn.attr('disabled',false);
			btn.removeClass('loading');
		},1000);
		
		
	});
	
	/* Filter */
	$('.filter-all-wrapper').overlayScrollbars({ });
	
	$(document).on('click','.filter-show-all',function(){
		let $parent = $(this).parent();
		$(this).addClass('d-none');
		$parent.find('.filter-hide-all').removeClass('d-none');
		$parent.find('.filter-all').removeClass('d-none');
		$parent.find('.filter-popular').addClass('d-none');
		$parent.find('input').focus();
	});
	$(document).on('click','.filter-hide-all',function(){
		let $parent = $(this).parent();
		$(this).addClass('d-none');
		$parent.find('.filter-show-all').removeClass('d-none');
		$parent.find('.filter-all').addClass('d-none');
		$parent.find('.filter-popular').removeClass('d-none');
	});
	$(document).on('keyup','.filter-search',function(){
		
		var elms, filter, txtValue;
		
		filter = $(this).val().toUpperCase();
		elms = $(this).next().find('.filter-link');
		
		for (i = 0; i < elms.length; i++) {
			txtValue = elms[i].textContent || elms[i].innerText;
			if (txtValue.toUpperCase().indexOf(filter) > -1) {
				$(elms[i]).show();
			} else {
				$(elms[i]).hide();
			}
		}
	});
	
	$('#filter-model .filter-link').on('click',function(){
		$('#filter-model .filter-link').removeClass('checked');
		$(this).addClass('checked');
		$('#filter-model-mod').removeClass('d-none');
	});
	
	$(document).on('click','.filter-open',function(){
		$('.filter-wrapper').show();
		$('.footer').css('z-index',0);
		$('body').css('overflow','hidden');
	});
	$(document).on('click','.filter-close',function(){
		$('.filter-wrapper').hide();
		$('.footer').css('z-index',1);
		$('body').css('overflow','auto');
	});
	
	/* last seen */
	if($('.last-seen').length) {
		$('.last-seen').slick({
		  slidesToShow: 3,
		  arrows: true,
		  dots: true,
		  prevArrow: '<i class="arrow fal fa-chevron-square-left"></i>',
		  nextArrow: '<i class="arrow fal fa-chevron-square-right"></i>',
		  responsive: [
			{
			  breakpoint: 1200,
			  settings: {
				 slidesToShow: 2,
			  }
			},
			{
			  breakpoint: 768,
			  settings: {
				 slidesToShow: 1,
				 arrows: false,
				
			  }
			}
		  ]
		});
	}
	
	 /* sticky sidebar */
	$('#sidebar').theiaStickySidebar({additionalMarginTop: 93, minWidth: 991 });
	
	/* product card slider */
	if($('.product-card-slider').length>0) {
			
			$('.product-card-slider').slick({
			  slidesToShow: 1,
			  slidesToScroll: 1,
			  arrows: false,
			  dots: false,
			  fade: true,
			  draggable: false,
			  asNavFor: '.product-card-slider-thumbs',
			});
			$('.product-card-slider').show();
			
			if($('.product-card-slider-thumbs').children().length>1) {
				$('.product-card-slider-thumbs').slick({
				  slidesToShow: 3,
				  slidesToScroll: 1,
				  asNavFor: '.product-card-slider',
				  dots: false,
				  focusOnSelect: true,
				  arrows: false,
				});
				$('.product-card-slider-thumbs').show();
			}
		}
	
	/* analogs | usage load */
	$(document).on('click', '.product-dop-watch', function() {
		
		$.fancybox.close();
		pushbar.open('mypushbar1');
		
		var article = $(this).data('article');
		var url = $(this).data('url');
		
		$('.pushbar-title span').text($(this).data('title'));
		$('.pushbar-content').empty().append('<div class="text-center pt-5"><span class="lds-ellipsis"><span></span><span></span><span></span><span></span></span></div>');
		
				
		setTimeout(function(){
			$.ajax({
			  method: "GET",
			  url: url,
			  dataType: "html",
			  success: function(data) {
				$('.pushbar-content').empty().append(data);  
			  }
			});
		},1000);
		
		
	});
	
	/* usage search */
	$(document).on('keyup','.usage-search',function(){

		
		var elms, filter, txtValue, cont;
		var arr = [];
		
		filter = $(this).val().toUpperCase();
		elms = $('.usage td');
		
		if(!filter) { $('.usage, .usage tr').show(); return; }
		
		for (i = 0; i < elms.length; i++) {
			txtValue = elms[i].textContent || elms[i].innerText;
						
			if (txtValue.toUpperCase().indexOf(filter) > -1) {
				$(elms[i]).parent().show();
				cont = $(elms[i]).parent().parent().parent().parent();
				arr.push(cont);
			} else {
				$(elms[i]).parent().hide();
			}
		}
		
		$('.usage').hide();
		for (i = 0; i < arr.length; i++) {
			arr[i].show();
		}
		
	});
	
	
	/*check order */
	$(document).on('change','.check-order',function(){
		if( $(this).prop('checked') ) {
			$.fancybox.open({
				src: '#check-order',
				touch: false,
				modal: true
			});
		} else {
			$('.vin').addClass('d-none').empty();
			$('input[name="vin"]').val('');
		}
	});
	$(document).on('click','.check-order-confirm',function(){
		var vin = $('input[name="vin"]').val();
		$('input[name="vin"]').removeClass('is-invalid');
		if(!vin) { $('input[name="vin"]').addClass('is-invalid'); }
		else {
			$.fancybox.close();
			$('.vin').removeClass('d-none').empty().text('VIN: '+vin);
		}
		
	});
	$(document).on('click','.check-order-cancel',function(){
		$.fancybox.close();
		$('.vin').addClass('d-none').empty();
		$('input[name="vin"]').val('');
		$('.check-order').prop('checked',false);
	});
	$(document).on('click','.vin-list li',function(){
		$('input[name="vin"]').val($(this).text());
		$('.check-order-confirm').trigger('click');
	});
	
	/* checkout */
	$(document).on('click','.checkout-del-type',function(){
		$('.checkout-del-type').removeClass('active');
		$(this).addClass('active');
		
		$('.checkout-del-tab').removeClass('active');
		$('.checkout-del-tab').eq($(this).index()).addClass('active');
	});
	
	 // CHECKBOXES / RADIO
    $(document).on('click', '.ch + label, .rd + label', function(){
      $(this).prev('input').click();
    });
	$('.agree').on('change', function() {
		if($(this).prop('checked')) {
			$( $(this).data('target') ).prop('disabled', false);
		} else {
			$( $(this).data('target') ).prop('disabled', true);
		}
	});
	
	
	// password eye
	$(".password-eye").click(function() {

	  $(this).toggleClass("active");
	  var input = $($(this).attr("toggle"));
	  if (input.attr("type") == "password") {
		input.attr("type", "text");
	  } else {
		input.attr("type", "password");
	  }
	});
	
	
	// repeat order
	$("#order-repeat").click(function() {
	  $.fancyConfirm({
		title     : "Повторить заказ?",
		message   : "Вы будете перенаправлены в корзину, её текущее содержимое будет очищено",
		okButton  : 'Подтвердить',
		noButton  : 'Отмена',
		callback  : function (value) {
		  if (value) {
			window.location.href= "/cart.html";

		  } 
		}
	  });

	});
	
	
	if($('.zoom-img').length) { $('.zoom-img').zoom(); }
	
	$('.vin-menu>li').on('click',function(){
		$(this).toggleClass('active');
	});
	
	
	$('.vin-catalog-toggle').on('click',function(){
		if($(this).hasClass('btn-reset')) {
			$(this).removeClass('btn-reset').addClass('btn-default');
			$(this).next().removeClass('d-none');
		} else {
			$(this).removeClass('btn-default').addClass('btn-reset');
			$(this).next().addClass('d-none');
		}
	});
	
	/* custom tabs */
	$(document).on('click','.my-tabs a',function(e){
		e.preventDefault();
		$('.my-tabs a').removeClass('active');
		$('.my-tabs-content').addClass('d-none');
		$(this).addClass('active');
		$($(this).attr('href')).removeClass('d-none');
	});
	
	/* tr clone */
	if($('.tr-clone').length) {
		
		var parent = $('.tr-clone').parent();
		var clone = $('<div>').append($('.tr-clone').clone()).html(); 
		
		$(document).on('click', '.tr-clone-add', function() {
			var article_fld = $(this).parent().parent().find('input[name="article[]"]');
			var quantity_fld = $(this).parent().parent().find('input[name="quantity[]"]');
			var article = article_fld.val();
			var quantity = parseInt(quantity_fld.val());
			
			article_fld.removeClass('is-invalid'); quantity_fld.removeClass('is-invalid');
						
			if(!article) { article_fld.addClass('is-invalid'); }
			if(!quantity) { quantity_fld.addClass('is-invalid'); }
			if(!article || !quantity) { return false; }
			
			parent.append(clone);
			$('.tr-clone-remove').removeClass('disabled');
		});
		
		$(document).on('click', '.tr-clone-remove', function() {
			if($('.tr-clone').length==1) { return; }
			if($('.tr-clone').length==2) { $('.tr-clone-remove').addClass('disabled'); }
			$(this).parent().parent().remove();
		})
		
	}
	
	
	
});

function Steps() {
	
	var that = this;
	var curStep;
	var nextStep;
	
	var init = function() {
		
		curStep = $('.step.active');
		nextStep = $('.step.active').next();
		
	}
	
	that.next = function() {
		
		var curStep = $('.step.active');
		var nextStep = $('.step.active').next();
	
		curStep.removeClass('active').addClass('validated');
		nextStep.addClass('active');
		
	}
	
	that.to = function() {
		
	}
	
	
	init();
	
}

function nextStep() {

	
	var curStep = $('.step.active') || $('.step.validated');
	if(!curStep.length) { curStep = $('.step.validated'); } 
	var nextStep = curStep.next();
	
	curStep.find('.btn-default').remove();
	
	curStep.removeClass('active').addClass('validated');
	nextStep.addClass('validated');
	
}



