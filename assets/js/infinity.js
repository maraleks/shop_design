

function InfinityCall() {
	
		
	var $this_obj = this;
	var stepH = 1;
	var stepM = 10;	
	
	var curhour;
	var curminute;
	var curday;
	var start = parseInt($('.infinity-call-button').attr('data-start-time'));
	var finish = parseInt($('.infinity-call-button').attr('data-finish-time'));
	var timer_bubble;
	var timer;
	var timerStart;
	var timeSpentOnSite = getTimeSpentOnSite();
	var stopCountingWhenWindowIsInactive = true; 

	init();
	
	// скрыть кнопку если находимся вне время работы	
	if (start > curhour || curhour >= finish) { $( ".infinity-call-button" ).css({'display' : 'none'}); return false; }
	
	// скрыть кнопку если выходной
	if(curday == 6 || curday == 0) { $( ".infinity-call-button" ).css({'display' : 'none'}); return false; }


	
	
	function init() {
		var now = new Date();
		curhour = now.getHours();
		curminute = now.getMinutes();
		curday = now.getDay();
		$("#callTimeH").val( prettify(curhour) );
		$("#callTimeM").val( prettify(curminute) );
		$('#calling').html('c '+ now.getHours()+':00'+' до '+finish+':00');
	}	
	
	// добавляем 0 ко времени если меньше 10
	function prettify(value){
		if(value < 10 ) { return '0'+value; }
		return value;
	}
	
	// опеределяем минмальное и максмилаьное значение минут
	function minmaxMinutes() {
		var hour_now = parseInt($('#callTimeH').val());
		var maxmin = 59;
		var minmin = (new Date()).getMinutes();
		
		if(hour_now > curhour) {
			minmin = 0;
		}	
		
		return [minmin,maxmin];
	}	
	
	// управление часами
	function hour(direction,customStep) {
		
		if(typeof customStep == "undefined") { customStep = stepH;}
		
		
		var maxhour = finish - 1;
			
		if(direction=='up') {
			
			  var hour_now = parseInt($('#callTimeH').val());
			 			  
			  if ( hour_now < maxhour ) {
				  $('#callTimeH').val( prettify(hour_now + customStep) );				
			  } else {
				  $('#callTimeH').val( prettify(curhour) );
			  }
		  
		} else {

			
			  var hour_now = parseInt($('#callTimeH').val());
			  
			  if ( hour_now > curhour) {
					$('#callTimeH').val( prettify(hour_now - customStep) );
			  } else {
					$('#callTimeH').val( prettify(curhour) );
			  }	

			  if( parseInt($('#callTimeH').val()) == curhour && parseInt($('#callTimeM').val()) < (new Date()).getMinutes() ) { minutes('down'); }	
								 
			
		}
	 
	}

	// управление минутами
	function minutes(direction,customStep) {
		
		if(typeof customStep == "undefined") { customStep = stepM; }
			
		if(direction=='up') {
					
			  var m = minmaxMinutes();
			  var maxmin = m[1];
			  var minmin = m[0];			  
			  
			  var minute_now = parseInt($('#callTimeM').val());
			  var increase = minute_now + customStep;
			  
				  if ( increase < maxmin ) {
						$('#callTimeM').val( prettify(increase) );
				  }  else {
						$('#callTimeM').val( prettify(minmin) );
				  }
	  
		} else {
				
				var m = minmaxMinutes();
				var maxmin = m[1];
				var minmin = m[0];
			  
				var minute_now = parseInt($('#callTimeM').val());
				var descrease = minute_now - customStep;
									  
						  if ( descrease > minmin) {
								$('#callTimeM').val( prettify(descrease) );
						  } else {
								$('#callTimeM').val( prettify(maxmin) );
						  }

		}
			
	};
		
	function inputValidator(e) {
		
		 if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return true;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            return false;
        }
		
		return true;
	}

	function show_call_bubble() {
		if($('.infinity-call-button').hasClass('paused')) { return false; }
		if(Cookies.get('show_call_bubble')) { return false; }
		$('.speech-bubble').show();
		$('.infinity-call-button').addClass('paused').addClass('blured');
		timer_bubble = setTimeout($this_obj.hide_call_bubble, 20000);
	}
	this.hide_call_bubble = function() {
		$('.speech-bubble').hide();
		$('.infinity-call-button').removeClass('paused').removeClass('blured');
		if (timer_bubble) {
				clearTimeout(timer_bubble);
				timer_bubble = 0;
			}
		// кука на один день не показывать bubble
		Cookies.set('show_call_bubble', 1, { expires: 1 });
	}
	function show_bubble_form() {
		$('.show-bubble-form').hide();
		$('.speech-bubble form').show();
	}
	function hide_bubble_form() {
		$('.show-bubble-form').show();
		$('.speech-bubble form').hide();
	}

	function getTimeSpentOnSite(){
		timeSpentOnSite = parseInt(Cookies.get('timeSpentOnSite'));
		timeSpentOnSite = isNaN(timeSpentOnSite) ? 0 : timeSpentOnSite;
		return timeSpentOnSite;
	}

	function startCounting(){
			timerStart = Date.now();
			timer = setInterval(function(){
				timeSpentOnSite = getTimeSpentOnSite()+(Date.now()-timerStart);
				Cookies.set('timeSpentOnSite',timeSpentOnSite);
				timerStart = parseInt(Date.now());
				// Convert to seconds
				var spent = parseInt(timeSpentOnSite/1000);
				//console.log(spent);
				if(spent>60) { show_call_bubble(); }
			},1000);
	}
	startCounting();

	if( stopCountingWhenWindowIsInactive ){
		
		if( typeof document.hidden !== "undefined" ){
			var hidden = "hidden", 
			visibilityChange = "visibilitychange", 
			visibilityState = "visibilityState";
		}else if ( typeof document.msHidden !== "undefined" ){
			var hidden = "msHidden", 
			visibilityChange = "msvisibilitychange", 
			visibilityState = "msVisibilityState";
		}
		var documentIsHidden = document[hidden];

		document.addEventListener(visibilityChange, function() {
			if(documentIsHidden != document[hidden]) {
				if( document[hidden] ){
					// Window is inactive
					clearInterval(timer);
				}else{
					// Window is active
					startCounting();
				}
				documentIsHidden = document[hidden];
			}
		});
	}
	
	this.callFancyOpen =  function () {
		
		$.fancybox.open({
			src: '#infinityCallback',
			afterClose: function() { $( ".infinity-call-button" ).show(); },
			beforeShow: function() { 
				
				init();
				
				$( ".infinity-call-button" ).hide(); 
				
			},
			afterShow: function() {
				
				var input = document.querySelector('.infinity-phone');
				input.addEventListener("input", mask, false);
				input.addEventListener("focus", mask, false);
				input.addEventListener("blur", mask, false);
				input.addEventListener("focusout", mask, false);
				
			},
			touch: false
		});
		
	}

		
	
	$(".speech-bubble").mouseenter(function(){
	if (timer_bubble) {
            clearTimeout(timer_bubble);
            timer_bubble = 0;
        }
	});
	$(".speech-bubble").mouseleave(function(){
		timer_bubble = setTimeout($this_obj.hide_call_bubble, 10000);
	});
	$('.show-bubble-form').click(function(){ show_bubble_form(); });
	$('.speech-bubble .close-bubble').click(function(){ $this_obj.hide_call_bubble(); });

	$('.minup').click(function () { minutes('up'); });
	$('.mindown').click(function () { minutes('down'); });
	
	$('.hourup').click(function () { hour('up'); });
	$('.hourdown').click(function () { hour('down'); });	
	
	// запретить ввод нечисловых значений
	$("#callTimeM, #callTimeH").keydown(function (e) {
		var result = inputValidator(e);
		if(!result) { e.preventDefault(); }
	});	
	
	// откатываемся если набраны неверные числовые значения
	$("#callTimeM").focusout(function (e) {
		var min = 59;
		var input = parseInt($(this).val());

		curhour = new Date().getHours();
		curminute = new Date().getMinutes();
		
		var input_hour = parseInt($("#callTimeH").val());
						
		if(  input > min ) { 
			minutes('down',input-min); 
		} else if ( input < curminute ) {
			if(input_hour == curhour ) { minutes('down'); }
		}	
		
	});
	$("#callTimeH").focusout(function (e) {
		var min = finish - 1;
		var input = parseInt($(this).val());
		
		curhour = new Date().getHours();
		

		if( input > min ) { 
			hour('down',input-min); 
		} else if ( input < curhour ) {
			hour('down');
		}	
		 		
	});	
	
		
	$('#callInfinityCallback').click(function(){ 
		
		$this_obj.callFancyOpen();
		Cookies.set('viewedOuibounceModal', 'true', { expires: 1 });
		Cookies.set('show_call_bubble', 1, { expires: 1 });
	
	});
	
	$( "#opentime" ).click(function() { 
		$(".time").slideDown();
	});
	
	$( "#now" ).click(function() { 
		$(".time").slideUp();
	});
	
	//обновляем время в форме кажду минуту
	var timerID = setInterval(function() {
		init();
	}, 120 * 1000);
		
};
			  

var Infinity_Call = new InfinityCall();



	